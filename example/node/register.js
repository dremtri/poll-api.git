const pollApi = require('poll-api')
// const axios = require('axios')

const fn = (err, res) => {
  if (err) {
    console.log(err, 'fn1')
    return
  }
  console.log(res.data, 'fn1')
}

const fn2 = (err, res) => {
  if (err) {
    console.log(err, 'fn2')
    return
  }
  console.log(res.data, 'fn2')
}

pollApi.registerEventType('test', {
  pollInterval: 5, // 轮询间隔时间
  pollCount: 3, // 轮询次数, 如果是 -1 则一致轮询
  delay: 0, // 延迟几秒后触发轮询
  axiosInstance: null, // axios 实例
  requestConfig: {
    url: 'https://api.jisuapi.com/area/province',
    // cancelToken: source.token
  }
})

pollApi.registerEventType('test1', {
  pollInterval: 5, // 轮询间隔时间
  pollCount: 3, // 轮询次数, 如果是 -1 则一致轮询
  delay: 0, // 延迟几秒后触发轮询
  axiosInstance: null, // axios 实例
  requestConfig: {
    url: 'https://api.jisuapi.com/area/province',
    // cancelToken: source.token
  }
})

// const source = axios.CancelToken.source()

// 开启轮询
pollApi.poll('test', fn)

pollApi.poll('test1', fn2)



setTimeout(() => {
  pollApi.destroy()
}, 5 * 1000)