const axios = require('axios')

const request = axios.create()

const getAxiosCancelTokenSource = () => {
  return axios.CancelToken.source()
}

const source1 = getAxiosCancelTokenSource()
const source2 = getAxiosCancelTokenSource()

const startRequest = () => {
  source1.cancel()
  request({
    url: 'https://api.jisuapi.com/area/province',
    cancelToken: source1.token
  }).catch(err => {
    console.log(err, 1)
  })
  source1.cancel()
  source2.cancel()
  request({
    url: 'https://api.jisuapi.com/area/province',
    cancelToken: source2.token
  }).catch(err => {
    console.log(err, 2)
  })
  source2.cancel()
}

startRequest()