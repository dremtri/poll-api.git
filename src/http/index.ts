import type { AxiosInstance, CancelTokenSource } from 'axios'
import axios from 'axios'
import { Nullable } from '@/index'

const axiosInstance: Nullable<AxiosInstance> = null

export const getAxiosInstance = (): AxiosInstance => {
  if (axiosInstance) {
    return axiosInstance
  }
  return axios.create()
}

export const getAxiosCancelTokenSource = (): CancelTokenSource => {
  return axios.CancelToken.source()
}
