import type { CreateOptions } from '@/index'

const DefaultPollConfig: CreateOptions = {
  pollInterval: 5, // 轮询间隔时间
  pollCount: -1, // 轮询次数, 如果是 -1 则一致轮询
  delay: 0, // 延迟几秒后触发轮询
  axiosInstance: null, // axios 实例
  reuse: false, // 是否单独运行一个task 如果为 true 多次调用此事件类型 只会创建一个task
  requestConfig: {}
}

export { DefaultPollConfig }
