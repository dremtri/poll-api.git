import type { CreateOptions, EventType, Fn, Nullable } from '@/index'
import TaskList from '@/taskList'
import Task from '@/task'

const taskListMap = new Map<EventType, TaskList>()

const add = (eventType: EventType, fn: Fn, opt: CreateOptions) => {
  let taskList = taskListMap.get(eventType)
  if (!taskList) {
    taskListMap.set(eventType, (taskList = new TaskList(eventType)))
  }
  const task = new Task(fn, opt)
  taskList.addTask(task)
}

const get = (eventType: EventType): Nullable<TaskList> => {
  return taskListMap.get(eventType) as Nullable<TaskList>
}

const clear = () => {
  taskListMap.forEach((taskList) => {
    taskList.clear()
  })
  taskListMap.clear()
}

export default {
  add,
  get,
  clear
}
