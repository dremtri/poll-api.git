import type { CreateOptions, EventType, Nullable } from '@/index'

const eventTypeMap = new Map<EventType, CreateOptions>()

// 以第一次 设置的参数为主
const add = (eventType: EventType, opt: CreateOptions): CreateOptions => {
  const eventTypeOpt = eventTypeMap.get(eventType)
  if (eventTypeOpt) {
    return eventTypeOpt
  }
  eventTypeMap.set(eventType, opt)
  return opt
}

const get = (eventType: EventType): Nullable<CreateOptions> => {
  return eventTypeMap.get(eventType) as Nullable<CreateOptions>
}

const deleteEventType = (eventType: EventType) => {
  eventTypeMap.delete(eventType)
}

const clear = () => {
  eventTypeMap.clear()
}

export default {
  add,
  get,
  clear,
  deleteEventType
}
