**English** | [中文](https://gitee.com/dremtri/poll-api/blob/master/README.zh_CN.md)

# poll-api
Poll-api is a library that helps developers manage API requests

**features**
- Asynchronous callback function
- Support delay or interval specified time request API

## Install & Usage
1. Install the poll-api

   ```sh
   # 使用 npm 安装
   npm install poll-api -S
   # 使用 yarn
   yarn add poll-api -S
   ```

2. Import poll-api and use

   ```js
   import PollApi from 'poll-api'
	// Open a polling API request
   PollApi.poll('poll', (err, res) =>{}, {
			pollInterval: 5,
			pollCount: -1,
			delay: 0,
			axiosInstance: null,
			requestConfig: {
				url: '/user',
				method: 'get',
				baseURL: 'http://localhost:8080/',
			 }
   })
   ```
   
## API
-   [poll](#poll)
	-   [Parameters](#parameters)
-   [off](#off)
	-   [Parameters](#parameters-1)
-   [clear](#clear)
    -   [Parameters](#parameters-2)
-   [destroy](#destroy)
-   [registerEventType](#registerEventType)
    -   [Parameters](#parameters-3)
-   [deleteEventType](#deleteEventType)
    -   [Parameters](#parameters-4)

### poll
Open a polling API request

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** EventType
-   `Fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** Callback function
- `option`
    - `pollInterval`: 5, // unit: s Default Value: 5
    - `pollCount`: -1, // Polling times. If the value is -1, polling is performed consistently Default Value: -1
    - `delay`: 0, //	Default Value: 0
    - `axiosInstance`: null, // Default Value: null; Reference [axiosInstance](https://www.axios-http.cn/docs/instance)
    - `requestConfig`: {} Reference [AxiosRequestConfig](https://www.axios-http.cn/docs/req_config)

### off
Turns off the polling API for the specified event type

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** EventType 
-   `Fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** Callback function

### clear
Closes all polling apis for the specified event type

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** EventType

### destroy
Clear all polling apis

### registerEventType
Clear all global registration events for the polling API. After successful registration, the next time the polling API is created, you do not need to pass the `options` parameter 

- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** EventType
- `option`
    - `pollInterval`: 5, // unit: s Default Value: 5
    - `pollCount`: -1, // Polling times. If the value is -1, polling is performed consistently Default Value: -1
    - `delay`: 0, //	Default Value: 0
    - `axiosInstance`: null, // Default Value: null; Reference [axiosInstance](https://www.axios-http.cn/docs/instance)
    - `requestConfig`: {} Reference [AxiosRequestConfig](https://www.axios-http.cn/docs/req_config)

### deleteEventType
Clears globally registered event types. This method does not know what is already running 

- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** EventType