**中文** | [English](https://gitee.com/dremtri/poll-api/blob/master/readme.md)

# poll-api
poll-api 是一个帮助开发人员管理Api请求的库

**功能**
- 异步回调函数
- 支持延迟或者间隔指定时间请求API

## 安装和使用
1. 安装 poll-api

   ```sh
   # 使用 npm 安装
   npm install poll-api -S
   # 使用 yarn
   yarn add poll-api -S
   ```

2. 引入 poll-api 并使用

   ```js
   import PollApi from 'poll-api'
	// 开启一个轮询API请求
   PollApi.poll('poll', (err, res) =>{}, {
			pollInterval: 5, // 轮询间隔时间
			pollCount: -1, // 轮询次数, 如果是 -1 则一致轮询
			delay: 0, // 延迟几秒后触发轮询
			axiosInstance: null, // axios 实例
			requestConfig: {
				url: '/user',
				method: 'get', // 默认值
				baseURL: 'http://localhost:8080/',
			 }
   })
   ```
   
## API
-   [poll](#poll)
	-   [Parameters](#parameters)
-   [off](#off)
	-   [Parameters](#parameters-1)
-   [clear](#clear)
    -   [Parameters](#parameters-2)
-   [destroy](#destroy)
-   [registerEventType](#registerEventType)
    -   [Parameters](#parameters-3)
-   [deleteEventType](#deleteEventType)
    -   [Parameters](#parameters-4)

### poll
开启一个 API 轮询请求

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** 事件类型
-   `Fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** 回调函数
- `option` 
    - `pollInterval`: 5, // 轮询间隔时间 单位 s 默认值: 5
    - `pollCount`: -1, // 轮询次数, 如果是 -1 则一致轮询 默认值: -1
    - `delay`: 0, // 延迟几秒后触发轮询	默认值: 0
    - `axiosInstance`: null, // axios 实例	默认值: null; 参考 [Axios实例](https://www.axios-http.cn/docs/instance)
    - `requestConfig`: {} 参考 [Axios请求配置](https://www.axios-http.cn/docs/req_config)

### off
关闭指定事件类型下的 轮询 API

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** 事件类型
-   `Fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** 回调函数

### clear
关闭指定事件类型下的 所有的轮询 API

#### Parameters
- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** 事件类型

### destroy
清除所有的轮询 API

### registerEventType
全局注册指定事件，注册成功后， 下次创建轮询 API 时可以直接不用传递 `options` 参数

- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** 事件类型
- `option` 
    - `pollInterval`: 5, // 轮询间隔时间 单位 s 默认值: 5
    - `pollCount`: -1, // 轮询次数, 如果是 -1 则一致轮询 默认值: -1
    - `delay`: 0, // 延迟几秒后触发轮询	默认值: 0
    - `axiosInstance`: null, // axios 实例	默认值: null; 参考 [Axios实例](https://www.axios-http.cn/docs/instance)
    - `requestConfig`: {} 参考 [Axios请求配置](https://www.axios-http.cn/docs/req_config)

### deleteEventType
清除全局注册的事件类型, 此方法不会清楚已经正在运行的

- `eventType` **([string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) \| [symbol](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol))** 事件类型