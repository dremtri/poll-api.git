import TaskList from '@/taskList'
import Task from '@/task'

describe('Test TaskList', () => {
  const eventType = 'PollApi'
  const opt = {
    requestConfig: {
      url: '/user',
      method: 'get', // 默认值
      baseURL: 'http://localhost:8080/',
    }
  }
  const fn = (err: any, res: any) => {
    // 空函数
    console.log(err)
  }
  const taskList = new TaskList(eventType)
  afterEach(() => {
    taskList.clear()
  })

  it('getTaskList', () => {
    const list = taskList.getTaskList()
    expect(list.length).toBe(0)
    const task = new Task(fn, opt)
    taskList.addTask(task)
    const list1 = taskList.getTaskList()
    expect(list1.length).toBe(1)
  })

  it('isExist', () => {
    expect(taskList.isExist(fn)).toBeFalsy()
    const task = new Task(fn, opt)
    taskList.addTask(task)
    expect(taskList.isExist(fn)).toBeTruthy()
  })

  it('getTask', () => {
    const task = new Task(fn, opt)
    taskList.addTask(task)
    expect(taskList.getTask(fn).length).toBe(1)
  })

  it('deleteTask', () => {
    const task = new Task(fn, opt)
    taskList.addTask(task)
    expect(taskList.getTask(fn).length).toBe(1)
    taskList.deleteTask(fn)
    expect(taskList.getTask(fn)).toBeUndefined()
  })
})