import EventTypeMap from '@/eventTypeMap'

describe('Test EventTypeMap', () => {
  const eventType = 'PollApi'
  afterEach(() => {
    EventTypeMap.clear()
  })
  it('add', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const opt2 = {
      requestConfig: {
        url: '/nameList',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const tempOpt = EventTypeMap.add(eventType, opt)
    expect(tempOpt).toBe(opt)
    const tempOpt2 = EventTypeMap.add(eventType, opt2)
    expect(tempOpt2).toBe(opt)
  })

  it('get', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const tempOpt = EventTypeMap.get(eventType)
    expect(tempOpt).toBeUndefined()
    EventTypeMap.add(eventType, opt)
    const tempOpt1 = EventTypeMap.get(eventType)
    expect(tempOpt1).toBe(opt)
    const opt2 = {
      requestConfig: {
        url: '/nameList',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    EventTypeMap.add(eventType, opt2)
    const tempOpt2 = EventTypeMap.get(eventType)
    expect(tempOpt2).toBe(opt)
  })

  it('deleteEventType', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    EventTypeMap.add(eventType, opt)
    const tempOpt1 = EventTypeMap.get(eventType)
    expect(tempOpt1).toBe(opt)
    const opt2 = {
      requestConfig: {
        url: '/nameList',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    EventTypeMap.deleteEventType(eventType)
    EventTypeMap.add(eventType, opt2)
    const tempOpt2 = EventTypeMap.get(eventType)
    expect(tempOpt2).toBe(opt2)
  })

  it('clear', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const opt2 = {
      requestConfig: {
        url: '/nameList',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    EventTypeMap.add(`${eventType}1`, opt)
    EventTypeMap.add(`${eventType}2`, opt2)
    const tempOpt = EventTypeMap.get(`${eventType}1`)
    const tempOpt2 = EventTypeMap.get(`${eventType}2`)
    expect(tempOpt).toBe(opt)
    expect(tempOpt2).toBe(opt2)
    EventTypeMap.clear()
    expect(EventTypeMap.get(`${eventType}1`)).toBeUndefined()
    expect(EventTypeMap.get(`${eventType}2`)).toBeUndefined()
  })
})