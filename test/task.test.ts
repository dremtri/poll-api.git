import Task from '@/task'

const fn = (err: any, res: any) => {
  // 空函数
  console.log(err)
}

describe('Test Task', () => {
  let task: Nullable<Task> = null
  const opt = {
    requestConfig: {
      url: '/user',
      method: 'get', // 默认值
      baseURL: 'http://localhost:8080/',
    }
  }
  beforeEach(() => {
    task = new Task(fn, opt)
  })
  afterEach(() => {
    task?.stopPollApi()
    task = null
  })

  it('stopPollApi', () => {
    expect(task?.isStop).toBeFalsy()
    task?.stopPollApi()
    expect(task?.isStop).toBeTruthy()
  })

  it('isContinue', () => {
    expect(task?.isContinue()).toBeTruthy()
    task?.stopPollApi()
    expect(task?.isContinue()).toBeFalsy()
  })
})

describe('Test Task Params', () => {
  it('pollCount', () => {
    const opt = {
      pollCount: 0,
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const task = new Task(fn, opt)
    expect(task.isStop).toBeTruthy()
    task.stopPollApi()
    expect(task.isStop).toBeTruthy()
  })

  // test.only('bootstrap', async () => {
  //   const opt = {
  //     pollCount: 3,
  //     requestConfig: {
  //       url: '/user',
  //       method: 'get', // 默认值
  //       baseURL: 'http://localhost:8080/',
  //     }
  //   }
  //   const task = new Task(fn, opt)
  //   expect(task.pollCount).toBe(3)
  //   await task.bootstrap()
  //   expect(task.pollCount).toBe(2)
  //   await task.bootstrap()
  //   expect(task.pollCount).toBe(1)
  //   await task.bootstrap()
  //   expect(task.pollCount).toBe(0)
  // })
})