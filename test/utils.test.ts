import { deepMerge } from '@/utils'
import { isObject } from '@/utils/is'

describe('utils', () => {
  it('utils => deepMerge', () => {
    const src = {
      name: 'mao',
      age: 12
    }
    const dest = {
      sex: 'man',
      weight: 50,
      nest: {
        a: '123'
      }
    }
    const result = deepMerge(src, dest)
    expect(result).toEqual({
      name: 'mao',
      age: 12,
      sex: 'man',
      weight: 50,
      nest: {
        a: '123'
      }
    })
    expect(result).toBe(src)
    const isSame = result.nest.a === dest.nest.a
    expect(isSame).toBeTruthy()
  })

  it('utils => is => isObject', () => {
    const obj = {}
    const test = null
    expect(isObject(obj)).toBeTruthy()
    expect(isObject(test)).toBeFalsy()
  })
})