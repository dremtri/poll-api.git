import TaskList from '@/taskList'
import TaskListMap from '@/taskListMap'

describe('Test taskListMap', () => {
  const eventType = 'PollApi'
  const fn = (err: any, res: any) => {
    // 空函数
    console.log(err)
  }
  it('add', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    const opt2 = {
      requestConfig: {
        url: '/nameList',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    TaskListMap.add(eventType, fn, opt)
    TaskListMap.add(eventType, fn, opt2)
    const taskList = TaskListMap.get(eventType)
    expect(taskList?.getTaskList().length).toBe(2)
  })

  it('get', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    TaskListMap.add(eventType, fn, opt)
    const taskList = TaskListMap.get(eventType)
    expect(taskList).toBeInstanceOf(TaskList)
  })

  it('clear', () => {
    const opt = {
      requestConfig: {
        url: '/user',
        method: 'get', // 默认值
        baseURL: 'http://localhost:8080/',
      }
    }
    TaskListMap.add(eventType, fn, opt)
    const taskList = TaskListMap.get(eventType)
    expect(taskList).toBeInstanceOf(TaskList)
    TaskListMap.clear()
    const taskList1 = TaskListMap.get(eventType)
    expect(taskList1).toBeUndefined()
  })
})